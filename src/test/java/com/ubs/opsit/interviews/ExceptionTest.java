package com.ubs.opsit.interviews;

import com.ubs.opsit.interviews.berlinerclock.BerlinerClock;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by tomas_000 on 08.10.2015.
 */
public class ExceptionTest {

    @Test(expected = IllegalArgumentException.class)
    public void testWrongArgunent() {
        new BerlinerClock("fake");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWrongOrder() {
        new BerlinerClock("60:12:60");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWrongMinutes() {
        new BerlinerClock("10:test:60");
    }

    @Test
    public void testCorrectBehave() {
        new BerlinerClock("10:10:10");
    }

    @Test
    public void testZero() {
        new BerlinerClock("00:00:00");
    }

    @Test(expected = IllegalArgumentException.class)
    public void test24() {
        new BerlinerClock("24:00:01");
    }


}
