package com.ubs.opsit.interviews.berlinerclock;

import static com.ubs.opsit.interviews.berlinerclock.BerlinerClockLampColor.RED;

/**
 * Class generates Berliner Clock Hours
 * Created by Tomasz �ygad�o on 08.10.2015.
 */
public class BerlinerClockHoursGenerator extends BerlinerClockTimeGenerator {
    @Override
    public String generateOutput(int hour) {
        int firstLevel = hour / 5;
        int secondLevel = hour % 5;
        return generateLine(firstLevel, 4, RED) + NEW_LINE + generateLine(secondLevel, 4, RED);

    }
}
