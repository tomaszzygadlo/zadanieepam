package com.ubs.opsit.interviews.berlinerclock;

/**
 * Enumeration of possible time type strategies.
 */
public enum BerlinerClockTimePart {
    SECONDS(new BerlinerClockSecondsGenerator(), 2, 0, 59),
    HOURS(new BerlinerClockHoursGenerator(), 0, 0, 24),
    MINUTES(new BerlinerClockMinutesGenerator(), 1, 0, 59);

    private final BerlinerClockTimeGenerator generator;
    private final int expectedPosition;
    private final int minimumValue;
    private final int maximumValue;

    BerlinerClockTimePart(BerlinerClockTimeGenerator generator, int expectedPosition, int minimumValue, int maximumValue) {
        this.generator = generator;
        this.expectedPosition = expectedPosition;
        this.minimumValue = minimumValue;
        this.maximumValue = maximumValue;
    }

    public int getExpectedPosition() {
        return expectedPosition;
    }

    public boolean checkValue(int val) {
        return val >= minimumValue && val <= maximumValue;
    }

    public String generateOutput(Integer value) {
        return generator.generateOutput(value);
    }
}
