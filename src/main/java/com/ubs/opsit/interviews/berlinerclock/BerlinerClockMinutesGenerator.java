package com.ubs.opsit.interviews.berlinerclock;

import java.util.Arrays;

import static com.ubs.opsit.interviews.berlinerclock.BerlinerClockLampColor.RED;
import static com.ubs.opsit.interviews.berlinerclock.BerlinerClockLampColor.YELLOW;

/**
 * Class generates Berliner Clock Minutes
 * Created by Tomasz �ygad�o on 08.10.2015.
 */
public class BerlinerClockMinutesGenerator extends BerlinerClockTimeGenerator {
    @Override
    public String generateOutput(int minute) {
        int firstLevel = minute / 5;
        int secondLevel = minute % 5;
        StringBuilder firstLine = new StringBuilder(generateLine(firstLevel, 11, YELLOW));
        Arrays.asList(2, 5, 8).stream().filter(x -> firstLine.charAt(x) == YELLOW.AS_CHAR).forEach(
                x -> firstLine.setCharAt(x, RED.AS_CHAR)
        );
        return firstLine.toString() + NEW_LINE + generateLine(secondLevel, 4, YELLOW);
    }
}
