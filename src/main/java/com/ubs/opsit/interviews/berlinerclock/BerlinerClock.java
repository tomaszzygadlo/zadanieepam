package com.ubs.opsit.interviews.berlinerclock;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.ubs.opsit.interviews.berlinerclock.BerlinerClockTimePart.*;

/**
 * Class responsible for Berliner Clock generation
 * Created by tomas_000 on 08.10.2015.
 */
public class BerlinerClock {
    private final Map<BerlinerClockTimePart, Integer> mapOfData = new EnumMap<>(BerlinerClockTimePart.class);

    public BerlinerClock(String aTime) {
        try {
            List<Integer> partsOfTime = Arrays.asList(aTime.split(":")).stream().map(Integer::parseInt).collect(Collectors.toList());
            mapOfData.putAll(Arrays.asList(BerlinerClockTimePart.values()).stream().collect(Collectors.toMap(p -> p, p -> partsOfTime.get(p.getExpectedPosition()))));
        } catch (RuntimeException re) {
            throw new IllegalArgumentException("Wrong clock input: " + aTime, re);
        }
        if (!(mapOfData.entrySet().stream().map(x -> x.getKey().checkValue(x.getValue())).reduce((x, y) -> x && y).get())) {
            throw new IllegalArgumentException("Not correct Inputs Order: " + aTime);
        }
        if (mapOfData.get(HOURS) > 23 && (mapOfData.get(MINUTES) > 0 || mapOfData.get(SECONDS) > 0)) {
            throw new IllegalArgumentException("Hour bigger than 24: " + aTime);
        }
    }

    public String getBerlinerTime() {
        return mapOfData.entrySet().stream()
                .map(x -> x.getKey().generateOutput(x.getValue()))
                .reduce((x, y) -> x + BerlinerClockTimeGenerator.NEW_LINE + y)
                .get();
    }

}
