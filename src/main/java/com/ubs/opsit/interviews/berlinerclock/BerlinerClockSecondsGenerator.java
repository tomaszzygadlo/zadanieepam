package com.ubs.opsit.interviews.berlinerclock;

import static com.ubs.opsit.interviews.berlinerclock.BerlinerClockLampColor.YELLOW;

/**
 * Class generates Berliner Clock Seconds
 * Created by Tomasz �ygad�o on 08.10.2015.
 */
public class BerlinerClockSecondsGenerator extends BerlinerClockTimeGenerator {
    @Override
    public String generateOutput(int second) {
        int firstLevel = 1 - second % 2;
        return generateLine(firstLevel, 1, YELLOW);
    }
}
