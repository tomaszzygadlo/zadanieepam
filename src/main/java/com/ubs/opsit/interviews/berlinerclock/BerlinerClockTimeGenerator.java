package com.ubs.opsit.interviews.berlinerclock;

import org.apache.commons.lang.StringUtils;

/**
 * Created by Tomasz �ygad�o on 08.10.2015.
 */
public abstract class BerlinerClockTimeGenerator {
    private Class c = getClass();
    public static final String NOT_BLINKED = "O";
    public static final String NEW_LINE = System.getProperty("line.separator");

    abstract String generateOutput(int time);

    String generateLine(int level, int levelSize, BerlinerClockLampColor color) {
        return StringUtils.repeat(color.AS_STRING, "", level) + StringUtils.repeat(NOT_BLINKED, "", levelSize - level);
    }

    @Override
    public boolean equals(Object o) {
        return getClass().equals(o);

    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
