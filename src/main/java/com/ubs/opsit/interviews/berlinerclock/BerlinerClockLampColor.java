package com.ubs.opsit.interviews.berlinerclock;

/**
 * Enumeraton of available lamp colors
 * Created by Tomasz �ygad�o on 08.10.2015.
 */
public enum BerlinerClockLampColor {
    RED('R'),
    YELLOW('Y');

    final char AS_CHAR;
    final String AS_STRING;

    BerlinerClockLampColor(char color) {
        AS_CHAR = color;
        AS_STRING = String.valueOf(color);
    }

}
