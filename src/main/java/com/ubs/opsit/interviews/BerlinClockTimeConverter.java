package com.ubs.opsit.interviews;

import com.ubs.opsit.interviews.berlinerclock.BerlinerClock;

/**
 * Created by Tomasz Żygadło on 06.10.2015.
 */
public class BerlinClockTimeConverter implements TimeConverter {

    @Override
    public String convertTime(String aTime) {
        BerlinerClock berlinerClock=new BerlinerClock(aTime);
        return berlinerClock.getBerlinerTime();
    }

}
